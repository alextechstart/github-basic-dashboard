const contentElement = document.getElementById("content");

const renderRepo = (content, message) => {
  const htmlContent = message
    ? `<h2>${message}</h2>`
    : content
    ? `
    <h1>${content.name}</h1>
    <p>Stars: <strong>${content.stargazers_count}</strong></p>
    <p>Latest commit: <strong>${dayjs(content.pushed_at).format(
      "DD MMMM YYYY HH:mm"
    )}</strong></p>
    <div class="user-wrapper">
        ${
          content.owner.avatar_url
            ? `<img src="${content.owner.avatar_url}" />`
            : ""
        }
        <h2><a href="${content.owner.html_url}">${content.owner.login}</a></h2>
    </div>
    ${
      content.__languages && Object.keys(content.__languages).length > 0
        ? `<p>Used languages: <strong>${Object.keys(content.__languages).join(
            ", "
          )}</strong></p>`
        : ""
    }
    <p>Description: <strong>${content.description}</strong></p>
    ${
      content.__contributors?.length > 0
        ? `<p>Top 10 most active contributors: <ul>${content.__contributors
            .map(
              (el) =>
                `<li><a href="${el.html_url}">${el.login} (${el.contributions})</a></li>`
            )
            .join("")}</ul></p>`
        : ""
    }
    `
    : "<h2>No data</h2>";
  contentElement.innerHTML = htmlContent;
};

window.onload = async () => {
  const urlParams = new URLSearchParams(window.location.search);
  const ownerParam = urlParams.get("owner");
  const repoParam = urlParams.get("repo");

  if (!ownerParam || !repoParam) {
    renderRepo(
      null,
      "Invalid query url. Please provide 'owner' and 'repo' params"
    );
    return;
  }

  renderRepo(null, "Loading...");

  try {
    const res = await axios.get(
      `https://api.github.com/repos/${ownerParam}/${repoParam}`
    );
    let readyContent = res.data;
    console.log("RES", res);

    if (readyContent.languages_url) {
      const res2 = await axios.get(readyContent.languages_url);
      readyContent.__languages = res2.data;
    }
    if (readyContent.contributors_url) {
      const res3 = await axios.get(readyContent.contributors_url);
      readyContent.__contributors = res3.data.slice(0, 10);
    }

    document.title = readyContent.name;
    renderRepo(readyContent);
  } catch (err) {
    renderRepo(null, err?.response?.data?.message || "Repository not found");
  }
};
