let mostPopularReposList = [];
let currentPage = 1;
const perPage = 10;
let cancelSource = null;

const listElement = document.getElementById("list");
const paginationElement = document.getElementById("pagination");
const searchElement = document.getElementById("search");

const renderList = (list, message) => {
  const htmlContent = message
    ? `<h2>${message}</h2>`
    : list?.length > 0
    ? `${list
        .map(
          (el) => `
      <div class="list-item">
        <a href="/inner?owner=${el.owner.login}&repo=${el.name}">
            <h2>${el.name}</h2>
        </a>
        <h4 class="list-item-details">
            <span class="list-item-details-item">Stars: ${
              el.stargazers_count
            }</span>
            <span class="list-item-details-item">Latest commit: ${dayjs(
              el.pushed_at
            ).format("DD MMMM YYYY HH:mm")}</span>
            <span class="list-item-details-item">
                <a href="${el.html_url}">GitHub</a>
            </span>
        </h4>
      </div>
      `
        )
        .join("")}`
    : "<h2>No data</h2>";
  listElement.innerHTML = htmlContent;
};

const renderPagination = (totalCount) => {
  const pagesCount = Math.min(Math.ceil(totalCount / perPage), 10);
  const searchText = searchElement.value;

  const htmlContent = [...new Array(pagesCount)]
    .map(
      (el, index) =>
        `<a href="/?page=${index + 1}&search=${searchText}"
            class="pagination-page${
              currentPage === index + 1 ? " active-page" : ""
            }">${index + 1}</a>`
    )
    .join("");

  paginationElement.innerHTML = htmlContent;
};

const addSearchOnChangeListener = (listener) => {
  searchElement.addEventListener("input", listener);
};

const updateUrlQuery = (query = "") => {
  const newUrl = window.location.origin + window.location.pathname + query;
  window.history.replaceState({ path: newUrl }, "", newUrl);
};

const searchApi = (value, page, shouldUpdateUrl) => {
  renderList(null, "Loading...");
  renderPagination(0);
  cancelSource = axios.CancelToken.source();

  if (shouldUpdateUrl) {
    updateUrlQuery(`?page=${page}&search=${value}`);
  }

  axios
    .get(
      `https://api.github.com/search/repositories?q=${value}&sort=stars&order=desc&page=${page}&per_page=${perPage}`,
      { cancelToken: cancelSource?.token }
    )
    .then((res) => {
      console.log("RES", res);
      renderList(res.data.items);
      renderPagination(res.data.total_count);
    })
    .catch((err) => {
      if (err?.response?.data?.message) {
        renderList(null, err?.response?.data?.message);
        renderPagination(0);
      }
    });
};

const debouncedSearchApi = _.debounce(searchApi, 400);

const handleSearch = (e) => {
  const searchText = e.target.value;
  currentPage = 1;
  cancelSource?.cancel?.();

  if (!searchText) {
    renderList(mostPopularReposList);
    renderPagination(0);

    debouncedSearchApi.cancel();
    updateUrlQuery("");

    return;
  }

  debouncedSearchApi(searchText, currentPage, true);
};

window.onload = () => {
  const urlParams = new URLSearchParams(window.location.search);
  const pageParam = urlParams.get("page");
  const searchParam = urlParams.get("search");

  renderList(null, "Loading...");
  axios
    .get(
      `https://api.github.com/search/repositories?q=stars:%3E1&sort=stars&per_page=${perPage}&page=1`
    )
    .then((res) => {
      mostPopularReposList = res.data.items;
      !searchParam && renderList(mostPopularReposList);
    })
    .catch((err) => {
      renderList(null, err.response.data.message);
      renderPagination(0);
    });

  addSearchOnChangeListener(handleSearch);

  if (pageParam && !isNaN(Number(pageParam))) {
    currentPage = Number(pageParam);
  }
  if (searchParam) {
    searchElement.value = searchParam;
    searchApi(searchParam, currentPage);
  }
};
