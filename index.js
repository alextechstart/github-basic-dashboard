const express = require("express");
const app = express();
const port = 3000;

let options = {
  dotfiles: "ignore", //allow, deny, ignore
  etag: true,
  extensions: ["htm", "html"],
  redirect: false,
};

app.use(express.static("public", options));

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
});
